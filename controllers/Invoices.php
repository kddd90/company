<?php namespace Monologophobia\Company\Controllers;

use Flash;
use BackendAuth;
use BackendMenu;
use Backend\Models\BrandSetting;
use Backend\Classes\Controller;
use Monologophobia\Company\Models\Invoice;
use Monologophobia\Company\Models\Settings;
use Response;
use Redirect;
use View;

class Invoices extends \Backend\Classes\Controller {

    public $implement = [
        'Backend.Behaviors.ListController'
    ];

    public $listConfig = 'config_list.yaml';

    public $requiredPermissions = ['monologophobia.company.invoices'];

    public function __construct() {
        parent::__construct();
        BackendMenu::setContext('Monologophobia.Company', 'company', 'invoices');
    }

    public function generate($id) {
        $this->addCss('/plugins/monologophobia/company/assets/css/invoice.css');
        $this->vars['invoice']  = Invoice::findOrFail($id);
        $this->vars['settings'] = Settings::instance();
        $this->vars['brand']    = BrandSetting::instance();
    }

    public function show($id) {
        $this->layoutPath[] = '~/plugins/monologophobia/company/layouts';
        $this->layout = 'blank';
        $this->addCss('/plugins/monologophobia/company/assets/css/invoice.css');
        $this->vars['invoice']  = Invoice::findOrFail($id);
        $this->vars['settings'] = Settings::instance();
        $this->vars['brand']    = BrandSetting::instance();
        return $this->makePartial('invoice');
    }

    public function listInjectRowClass($record, $definition = null) {
        if ($record->amount_due == 0) {
            return 'positive';
        }
        else {
            if ($record->isOverdue()) {
                return 'negative';
            }
        }
    }

    public function onMarkAsPaid() {
        if (($checkedIds = post('checked')) && is_array($checkedIds) && count($checkedIds)) {
            foreach ($checkedIds as $id) {
                $invoice = Invoice::find($id);
                $invoice->pay($invoice->amount_due * 100, 0, 'Internal');
            }
            return $this->listRefresh();
        }
        if (post('id')) {
            $invoice = Invoice::find(intval(post('id')));
            $invoice->pay($invoice->amount_due * 100, 0, 'Internal');
            return Redirect::refresh();
        }
    }

    public function onEmail($id) {
        $message = filter_var(post('message', ''), FILTER_SANITIZE_STRING);
        $invoice = Invoice::findOrFail(intval($id));
        $invoice->sendInvoice($message);
        \Flash::success('Invoice Emailed');
        return Redirect::refresh();
    }

    // Exports a CSV of selected invoices
    public function index_onExport() {

        // check to make sure some ids have been passed
        if (($checkedIds = post('checked')) && is_array($checkedIds) && count($checkedIds)) {

            $not_found = false;
            
            // Set up a CSV with headers for export
            // Headers and data specific to SAGE
            // http://opentuition.com/sage/uploading-service-invoice-from-csv-file/
            $output = [];
            array_push($output, array(
                                    'Type',
                                    'Account Reference',
                                    'Nominal A/C Ref',
                                    'Department Code',
                                    'Date',
                                    'Reference',
                                    'Details',
                                    'Net Amount',
                                    'Tax Code',
                                    'Tax Amount'
                                )
                       );

            foreach ($checkedIds as $id) {
                
                $invoice = Invoice::find($id);

                if ($invoice) {

                    // Get invoice description
                    $details = '';
                    foreach ($invoice->projects as $project) {
                        $details .= $project->type . ':  ' . $project->name . ' (' . $project->price .') - ' . $project->description . '\r\n';
                    }

                    // Add items to CSV
                    array_push($output, array(
                                            'SI',
                                            $invoice->client->internal_name,
                                            '4000',
                                            '',
                                            Date('d/m/Y', strtotime($invoice->created_at)),
                                            $invoice->id,
                                            $details,
                                            $invoice->projects->sum('price'),
                                            'T9',
                                            '0.00'
                                        )
                              );

                }

                // If the Invoice wasn't found, set a flag to inform the user
                // This doesn't stop the loop as it's more of a warning than anything else
                else {
                    $not_found = true;
                }

            }

            // Inform the user in case any weren't found or if success
            if (!$not_found) {
                
                // Return the csv back to the ajax request
                Flash::success('Exporting...');
                return ['result' => $output];
                die();
                
            } else {
                Flash::error('Some items couldn\'t be found or had errors.');
            }

            

        }
        else {
            Flash::error('Couldn\'t find any Invocies to export');
        }

        return $this->listRefresh();

    }

}