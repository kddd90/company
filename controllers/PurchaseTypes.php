<?php namespace Monologophobia\Company\Controllers;

use Flash;
use BackendAuth;
use BackendMenu;
use Backend\Classes\Controller;
use Monologophobia\Company\Models\PurchaseType;

class PurchaseTypes extends \Backend\Classes\Controller {

    public $implement = [
        'Backend.Behaviors.ListController',
        'Backend.Behaviors.FormController'
    ];

    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public $requiredPermissions = ['monologophobia.company.salepurchasetypes'];

    //public $bodyClass = 'compact-container';

    public function __construct() {
        parent::__construct();
        BackendMenu::setContext('Monologophobia.Company', 'company', 'purchasetypes');
    }
    
    /**
     * Deleted checked users.
     */
    public function index_onDelete()
    {
        if (($checkedIds = post('checked')) && is_array($checkedIds) && count($checkedIds)) {
            foreach ($checkedIds as $id) {
                if (!$type = PurchaseType::find($id)) continue;
                $type->delete();
            }
            Flash::success('Deleted Successfully');
        }
        else {
            Flash::error('Error Deleting');
        }
        return $this->listRefresh();
    }

    public function onSave($record) {
        print_r($record); die();
        //$this->checkActive();
        return $this->asExtension('FormController')->update($recordId, $context);
    }
    
}