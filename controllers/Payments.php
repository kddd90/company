<?php namespace Monologophobia\Company\Controllers;

use Flash;

use BackendAuth;
use BackendMenu;
use Backend\Classes\Controller;

use Monologophobia\Company\Models\Payment;

class Payments extends \Backend\Classes\Controller {

    public $implement = [
        'Backend.Behaviors.ListController',
        'Backend.Behaviors.FormController'
    ];

    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public $requiredPermissions = ['monologophobia.company.payments'];

    public function __construct() {
        parent::__construct();
        BackendMenu::setContext('Monologophobia.Company', 'company', 'payments');
    }

    public function index_onDelete() {
        if (($checkedIds = post('checked')) && is_array($checkedIds) && count($checkedIds)) {
            foreach ($checkedIds as $id) {
                $level = Payment::findOrFail($id);
                $level->delete();
            }
            Flash::success('Deleted Successfully');

        }
        else {
            Flash::error('Couldn\'t find the IDs');
        }
        return $this->listRefresh();
    }

    public function export() {
        return $this->listExportCsv(array('filename' => 'payments.csv'));
    }

}
