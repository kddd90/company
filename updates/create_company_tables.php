<?php namespace Monologophobia\Company\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreateCompanyTables extends Migration {

    public function up() {

        Schema::create('monologophobia_company_clients', function($table) {
            $table->increments('id');
            $table->text('name')->unique;
            $table->text('internal_name')->nullable();
            $table->text('address')->nullable();
            $table->integer('co_number')->nullable();
            $table->text('contact')->nullable();
            $table->text('contact_email')->nullable();
            $table->text('contact_phone')->nullable();
            $table->integer('terms')->default(30);
            $table->integer('active')->default(1);
            $table->text('created_at')->nullable();
            $table->text('updated_at')->nullable();
        });

        Schema::create('monologophobia_company_invoices', function($table) {
            $table->increments('id');
            $table->integer('client_id');
            $table->text('created_at')->nullable();
            $table->text('updated_at')->nullable();
            
            $table->foreign('client_id')->references('id')->on('monologophobia_company_clients');

        });

        Schema::create('monologophobia_company_purchase_types', function($table) {
            $table->increments('id');
            $table->text('name');
            $table->text('created_at')->nullable();
            $table->text('updated_at')->nullable();
        });

        Schema::create('monologophobia_company_sale_types', function($table) {
            $table->increments('id');
            $table->text('name');
            $table->text('created_at')->nullable();
            $table->text('updated_at')->nullable();
        });

        Schema::create('monologophobia_company_projects', function($table) {
            $table->increments('id');
            $table->text('name');
            $table->text('description')->nullable();
            $table->integer('type')->default(1);
            $table->integer('client_id');
            $table->double('price', 5, 2)->default('40.00');
            $table->integer('assigned_to')->nullable();
            $table->integer('complete')->default(0);
            $table->integer('invoice_id')->nullable();
            $table->text('created_at')->nullable();
            $table->text('updated_at')->nullable();

            $table->index('client_id');
            $table->index('invoice_id');

            $table->foreign('client_id')->references('id')->on('monologophobia_company_clients');
            $table->foreign('invoice_id')->references('id')->on('monologophobia_company_invoices');
            $table->foreign('type')->references('id')->on('monologophobia_company_sale_types');

        });

        Schema::create('monologophobia_company_projects_items', function($table) {
            $table->increments('id');
            $table->text('name');
            $table->text('description')->nullable();
            $table->integer('assigned_to')->nullable();
            $table->integer('ecd')->nullable();
            $table->text('created_at')->nullable();
            $table->text('updated_at')->nullable();
        });

    }

    public function down() {
        Schema::dropIfExists('monologophobia_company_clients');
        Schema::dropIfExists('monologophobia_company_invoices');
        Schema::dropIfExists('monologophobia_company_projects');
        Schema::dropIfExists('monologophobia_company_project_items');
        Schema::dropIfExists('monologophobia_company_purchase_types');
        Schema::dropIfExists('monologophobia_company_sale_types');
    }

}