<?php namespace Monologophobia\Company\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class Notify extends Migration {

    public function up() {
        Schema::table('monologophobia_company_clients', function($table) {
            $table->integer('notify')->default(0);
        });
    }

    public function down() {
        Schema::table('monologophobia_company_clients', function($table) {
            $table->dropColumn('notify');
        });
    }

}