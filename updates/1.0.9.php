<?php namespace Monologophobia\Company\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class OnePointZeroPointNine extends Migration {

    public function up() {

        Schema::table('monologophobia_company_invoices', function($table) {
            $table->double('paid_amount', 5, 2)->default(0);
        });

    }

    public function down() {
        Schema::table('monologophobia_company_invoices', function($table) {
            $table->dropColumn('paid_amount');
        });
    }

}