<?php namespace Monologophobia\Company\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class OnePointZeroPointSix extends Migration {

    public function up() {

        Schema::table('monologophobia_company_invoices', function($table) {
            $table->text('unique_id');
        });

        Schema::table('monologophobia_company_clients', function($table) {
            $table->renameColumn('contact_email', 'email');
            $table->softDeletes();
            $table->string('password');
            $table->string('activation_code')->nullable()->index();
            $table->string('persist_code')->nullable();
            $table->string('reset_password_code')->nullable()->index();
            $table->text('permissions')->nullable();
        });
    }

    public function down() {
        Schema::table('monologophobia_company_invoices', function($table) {
            $table->dropColumn('unique_id');
        });
        Schema::table('monologophobia_company_clients', function($table) {
            $table->renameColumn('email', 'contact_email');
            $table->dropColumn('deleted_at');
            $table->dropColumn('password');
            $table->dropColumn('activation_code');
            $table->dropColumn('persist_code');
            $table->dropColumn('reset_password_code');
            $table->dropColumn('permissions');
        });
    }

}