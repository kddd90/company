<?php namespace Monologophobia\Company\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class OnePointZeroPointTen extends Migration {

    public function up() {

        Schema::table('monologophobia_company_invoices', function($table) {
            $table->string('payment_reference')->nullable();
        });

    }

    public function down() {
        Schema::table('monologophobia_company_invoices', function($table) {
            $table->dropColumn('payment_reference');
        });
    }

}