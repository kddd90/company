<?php namespace Monologophobia\Company\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreateCompanyTables extends Migration {

    public function up() {

        Schema::create('monologophobia_company_payments', function($table) {
            $table->increments('id');
            $table->integer('purchase_type_id')->index()->unsigned();
            $table->foreign('purchase_type_id')->references('id')->on('monologophobia_company_purchase_types')->onDelete('cascade');
            $table->decimal('amount', 5, 2);
            $table->timestamps();
            $table->softDeletes();
        });

    }

    public function down() {
        Schema::dropIfExists('monologophobia_company_payments');
    }

}