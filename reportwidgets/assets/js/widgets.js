function refreshOngoing() {
    if ($('#ongoing').length > 0) {
        $('.control-filter select.client').trigger('change');
    }
    var focus_field = document.getElementById('Form-field-ProjectsModel-name');
    focus_field.focus();
}

$(document).ready(function() {

    var table = '#ongoing table';

    $(document).on('change', '#scope-incomplete', function(e) {
        e.stopPropagation();
        checkIncompletes();
    });

    $(document).on('click', table + ' tbody tr td.toggle', function(e) {
        var parent = $(this).closest('tr');
        var project_id = $(parent).data('id');
        $(parent).find('td.list-cell-name-name').append('<div class="icon-loading" style="padding-left: 1rem"><i class="icon-spinner fa-spin"></i></div>');
        $.oc.stripeLoadIndicator.show();
        $(this).request('onToggle', {
            data: {id: project_id },
            success: function(data) {
                $(parent).removeClass('suspended').removeClass('complete');
                if (data.status === 1) $(parent).addClass('complete');
                if (data.status === 2) $(parent).addClass('suspended');
                updateOverviewCharts(data);
            },
            error: function() {
                alert('Something went wrong. It\'s probably your fault.');
            },
            complete: function() {
                $(parent).find('.icon-loading').remove();
                $.oc.stripeLoadIndicator.hide();
            }
        });
    });

    $(document).on('click', table + ' thead th', function() {
        var element   = this;
        var client_id = $('.control-filter select.client').val();
        var direction;
        if ($(this).hasClass('sort-desc')) direction = 'asc';
        if ($(this).hasClass('sort-asc'))  direction = 'desc';
        if (direction) {
            $.oc.stripeLoadIndicator.show();
            var column = $(this).data('column');
            $(this).request('onSort', {
                data: {'sortDirection': direction, 'sortColumn': column, 'clientId': client_id},
                success: function(data) {
                    populateTable(data);
                    $(element).removeClass('sort-asc').removeClass('sort-desc').addClass('sort-' + direction);
                },
                error: function() {
                    alert('Something went wrong. It\'s probably your fault.');
                },
                complete: function() {
                    $.oc.stripeLoadIndicator.hide();
                }
            });
        }
    });

    $(document).on('change', '.control-filter select.client', function() {
        var client_id = $(this).val();
        if (!client_id) client_id = 0;
        $.oc.stripeLoadIndicator.show();
        $(this).request('onSort', {
            data: {'clientId': client_id},
            success: function(data) {
                populateTable(data);
                updateOverviewCharts(data);
            },
            error: function() {
                alert('Something went wrong. It\'s probably your fault.');
            },
            complete: function() {
                $.oc.stripeLoadIndicator.hide();
            }
        });
    });

    function populateTable(data) {
        $(table).find('tbody').html('');
        for (var i in data) {
            if (data[i].id) {
                var complete = '';
                if (data[i].complete == 1) complete = ' class="complete"';
                if (data[i].complete == 2) complete = ' class="suspended"';
                $(table).find('tbody').append('<tr' + complete + ' data-id=' + data[i].id + '"><td>' + data[i].client_name + '</td><td>' + data[i].name + '</td><td>&pound;' + Number(data[i].price).toFixed(2) + '</td><td>' + data[i].created_at + '</td><td class="edit"><a href="/backend/monologophobia/company/projects/update/' + data[i].id + '" title="Edit">Edit</a></tr>');
            }
        }
    }

    function checkIncompletes() {
        if ($('#scope-incomplete').is(':checked')) {
            $(table).find('tr.complete').hide();
        } else {
            $(table).find('tr.complete').show();
        }
    }

    function updateOverviewCharts(data) {
        if (data.chart) {

            if ($('#overview-projects').length > 0) {
                $('#overview-projects ul li.chart-complete span').html(data.chart.complete);
                $('#overview-projects ul li.chart-ongoing span').html(data.chart.ongoing);
                $('#overview-projects .chart-legend tr:first td.value').html(data.chart.complete);
                $('#overview-projects .chart-legend tr:last td.value').html(data.chart.ongoing);
            }

            if ($('#overview-goal').length > 0) {
                $('#overview-goal .goal-meter-indicator span').animate({'height': data.chart.goal + '%'});
                $('#overview-goal .percentage').html(data.chart.goal + '%');
            }

        }
    }

});
