<?php namespace Monologophobia\Company\ReportWidgets;

use BackendAuth;
use Backend\Classes\ReportWidgetBase;
use Monologophobia\Company\Models\Project;
use Monologophobia\Company\Models\Client;

class Ongoing extends ReportWidgetBase {

    protected $defaultAlias = 'monologophobia_widget_ongoing';

    public function render() {

        try {
            $this->loadData();
        }
        catch (Exception $ex) {
            $this->vars['error'] = $ex->getMessage();
        }

        $this->addJs('/plugins/monologophobia/company/reportwidgets/assets/js/widgets.js');

        return $this->makePartial('widget');

    }

    public function defineProperties() {
        return [
            'title' => [
                'title'             => 'Label',
                'default'           => 'Ongoing Projects Overview',
                'type'              => 'string',
                'validationPattern' => '^.+$',
                'validationMessage' => 'backend::lang.dashboard.widget_title_error',
            ]
        ];
    }

    protected function loadData($sort_column = 'created_at', $sort_direction = 'desc', $client_id = null) {

        $this->vars['user'] = BackendAuth::getUser();

        $this->vars['clients'] = [];
        foreach (Client::orderBy('name')->get() as $client) {
            $this->vars['clients'][$client->attributes['id']] = $client->attributes;
        }

        $this->vars['projects'] = [];
        if (!$client_id) {
            $projects = Project::whereNull('invoice_id')
                                ->where('assigned_to', '=', $this->vars['user']-> id)
                                // I hate Andy Dickinson
                                ->orderByRaw('CASE complete WHEN 2 then 2 WHEN 1 then 3 WHEN 0 THEN 1 end')
                                ->orderByRaw("lower($sort_column) " . $sort_direction)
                                ->get();
        } else {
            $projects = Project::whereNull('invoice_id')
                                ->where('assigned_to', '=', $this->vars['user']-> id)
                                ->where('client_id', '=', $client_id)
                                ->orderByRaw('CASE complete WHEN 2 then 2 WHEN 1 then 3 WHEN 0 THEN 1 end')
                                ->orderByRaw("lower($sort_column) " . $sort_direction)
                                ->get();
        }

        foreach ($projects as $project) {
            $project->attributes['client_name'] = $this->vars['clients'][$project->attributes['client_id']]['name'];
            array_push($this->vars['projects'], $project->attributes);
        }

    }

    public function onToggle() {
        $id = filter_var(post('id'), FILTER_SANITIZE_NUMBER_INT);
        $project = Project::where('id', '=', $id)->first();
        $complete = 0;
        if ($project->complete == 0) $complete = 2;
        if ($project->complete == 2) $complete = 1;
        if ($project->complete == 1) $complete = 0;
        if (Project::where('id', '=', $id)->update(array('complete' => $complete))) {
            $this->vars['projects']['chart']  = $this->getChartDetails();
            $this->vars['projects']['status'] = $complete;
            return $this->vars['projects'];
        }
        return false;
    }

    public function onSort() {

        $sort_column    = input('sortColumn', 'created_at');
        $sort_direction = input('sortDirection', 'desc');
        $client_id      = filter_var(input('clientId', 0), FILTER_SANITIZE_NUMBER_INT);
        $this->loadData($sort_column, $sort_direction, $client_id);

        $this->vars['projects']['chart'] = $this->getChartDetails();

        return $this->vars['projects'];

    }

    // Get chart recalcs
    private function getChartDetails() {
        $object = [];
        $object['ongoing']  = Project::where('complete', '=', '0')->whereNull('invoice_id')->count();
        $object['complete'] = Project::where('complete', '=', '1')->whereNull('invoice_id')->count();
        $object['goal']     = Project::calculateWeeklyGoalPercentageFromCompletes();
        return $object;
    }
}
