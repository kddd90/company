<?php namespace Monologophobia\Company\ReportWidgets;

use Backend\Classes\ReportWidgetBase;
use Flash;
use Monologophobia\Company\Models\Client;
use Monologophobia\Company\Models\Project;
use Monologophobia\Company\Models\SalesType;
use BackendAuth;

class NewProject extends ReportWidgetBase {

    // Ongoing had same alias?
    protected $defaultAlias = 'monologophobia_widget_newproject';

    public function render() {

        try {
            $this->loadData();
        }
        catch (Exception $ex) {
            $this->vars['error'] = $ex->getMessage();
        }

        $this->addJs('/plugins/monologophobia/company/reportwidgets/assets/js/widgets.js');

        return $this->makePartial('form');

    }

    public function defineProperties() {
        return [
            'title' => [
                'title'             => 'Label',
                'default'           => 'New Quick Project',
                'type'              => 'string',
                'validationPattern' => '^.+$',
                'validationMessage' => 'backend::lang.dashboard.widget_title_error',
            ]
        ];
    }

    protected function loadData() {
        $this->vars['clients'] = Client::orderByRaw('(id = 1) DESC, name')->where('active', true)->get();
        $this->vars['types']   = SalesType::get();
    }

    public function onSubmit() {

        $page     = post('ProjectsModel');
        $projects = new Project;
        $projects->name        = filter_var($page['name'], FILTER_SANITIZE_STRING);
        $projects->price       = filter_var($page['price'], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
        $projects->client_id   = filter_var($page['client_id'], FILTER_SANITIZE_NUMBER_INT);
        $projects->description = filter_var($page['description'], FILTER_SANITIZE_STRING);
        $projects->type        = filter_var($page['saletypes'], FILTER_SANITIZE_NUMBER_INT);
        $projects->assigned_to = BackendAuth::getUser()->id;
        
        if ($projects->save()) {
            Flash::success('Quick Project Created!');
        } else {
            Flash::error('Error!');
        }

    }

}
