<?php namespace Monologophobia\Company\Models;

use \October\Rain\Database\Traits\SoftDeleting;
use \October\Rain\Database\Model;

class Payment extends Model {

    use \October\Rain\Database\Traits\Nullable;
    use SoftDeleting;

    public $table = 'monologophobia_company_payments';
    public $timestamps  = true;
    protected $dates    = ['created_at', 'updated_at', 'deleted_at', 'paid_date'];
    protected $nullable = ['spent_at', 'spent_on', 'invoice_id'];    

    // Any validation for incoming data
    use \October\Rain\Database\Traits\Validation;
    public $rules = [
        'purchase_type_id' => 'required|integer',
        'amount'           => 'required|numeric',
        'paid_date'        => 'required'
    ];

    public $belongsTo = [
        'purchase_type' => ['Monologophobia\Company\Models\PurchaseType', 'key' => 'purchase_type_id'],
        'invoice' => ['Monologophobia\Company\Models\Invoice', 'key' => 'invoice_id']
    ];

    public $belongsToMany = [
    ];

    public $hasMany = [
    ];

    public $hasOne = [
    ];

    public function getInvoiceIdOptions() {
        $array = ["0" => "No Related Invoice"];
        foreach (Invoice::lists('id') as $id) {
            $array[$id] = $id;
        }
        return $array;
    }

    public function beforeValidate() {
    }

    public function beforeSave() {
        $this->paid_date = $this->paid_date->format('Y-m-d');
    }

    public function afterDelete() {
    }

    public function beforeUpdate() {
    }

    public function afterUpdate() {
    }

}
