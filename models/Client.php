<?php namespace Monologophobia\Company\Models;

use \October\Rain\Database\Model;

class Client extends Model {

    use \October\Rain\Database\Traits\SoftDelete;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'monologophobia_company_clients';

    use \October\Rain\Database\Traits\Validation;
    public $rules = [
        'name'  => 'required|min:3',
        'terms' => 'required|min:1',
        'email' => 'required|email',
    ];

    public $hasMany = [
        'projects' => ['Monologophobia\Company\Models\Project', 'key' => 'client_id'],
        'invoices' => ['Monologophobia\Company\Models\Invoice', 'key' => 'client_id']
    ];

    public $hasManyThrough = [
        'invoiced_projects' => [
            'Monologophobia\Company\Models\Project',
            'key'        => 'client_id',
            'through'    => 'Monologophobia\Company\Models\Invoice',
            'throughKey' => 'invoice_id'
        ],
    ];

    public $belongsToMany = [];
    public $belongsTo = [];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    public function getBalanceAttribute() {
        $balance = $this->invoiced_projects->sum('price');
        $balance = number_format($balance, 2);
        $balance = \Lang::get('monologophobia.company::lang.app.currency') . $balance;
        return $balance;
    }

}