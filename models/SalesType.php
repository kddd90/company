<?php namespace Monologophobia\Company\Models;

use \October\Rain\Database\Model;

class SalesType extends Model {

    /**
     * @var string The database table used by the model.
     */
    public $table = 'monologophobia_company_sale_types';

    use \October\Rain\Database\Traits\Validation;
    public $rules = [
        'name' => 'required|min:3'
    ];

    public $hasMany = [
        'projects' => ['Monologophobia\Company\Models\Project', 'key' => 'type']
    ];

}