<?php namespace Monologophobia\Company\Models;

use Model;
use Cms\Classes\Page;
use Monologophobia\Company\Models\PurchaseType;

class Settings extends Model {

    public $implement = ['System.Behaviors.SettingsModel'];

    // A unique code
    public $settingsCode = 'mono_company_settings';

    // Reference to field configuration
    public $settingsFields = 'fields.yaml';

    /**
     * Gets the relevant Stripe publishable key if we're in live or sandbox mode
     * @return Array ["secret_key", "publishable_key"]
     */
    public static function getStripeKeys() {
        $live = boolval(Settings::get('live'));
        return array(
            "secret_key"      => ($live == 1 ? Settings::get('stripe_secret') : Settings::get('stripe_sandbox_secret')),
            "publishable_key" => ($live == 1 ? Settings::get('stripe_publishable') : Settings::get('stripe_sandbox_publishable'))
        );
    }

    public function getInvoicePaymentsPageOptions() {
        return Page::sortBy('baseFileName')->lists('title', 'url');
    }

    public function getDefaultPaymentTypeForFeesOptions() {
        return PurchaseType::lists('name', 'id');
    }

}
