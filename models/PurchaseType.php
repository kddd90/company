<?php namespace Monologophobia\Company\Models;

use \October\Rain\Database\Model;

class PurchaseType extends Model {

    /**
     * @var string The database table used by the model.
     */
    public $table = 'monologophobia_company_purchase_types';

    use \October\Rain\Database\Traits\Validation;
    public $rules = [
        'name' => 'required|min:3'
    ];

    public $hasMany = [
        'purchases' => ['Monologophobia\Company\Models\Purchase', 'key' => 'purchase_type_id']
    ];

}